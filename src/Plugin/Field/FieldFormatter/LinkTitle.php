<?php

namespace Drupal\link_title_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\link\Plugin\Field\FieldFormatter\LinkFormatter;
use Drupal\Component\Utility\Html;

/**
 * Plugin implementation of the 'link' formatter.
 *
 * @FieldFormatter(
 *   id = "link_title",
 *   label = @Translation("Link Title"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class LinkTitle extends LinkFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return ['trim_length' => ''] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item)  {
      if (!empty($item->title)) {
        $element[$delta] = [
          '#markup' => Html::escape($item->title),
        ];
      }
    }
    return $element;
  }

}
