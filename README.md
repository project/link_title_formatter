CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
------------

The link title formatter module allows you to just display the link title field without the link. This module is useful when we use UI Patterns to display the link as button, which accepts two separate fields to render the button (URL & Label).

You can use the Display Copy Field module to make a copy of the link field. Use one field for rendering just the URL and use the copy field for displaying just the link title. 

REQUIREMENTS
------------

This module requires no additional contrib modules.

INSTALLATION
------------

Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7
for further information.

CONFIGURATION
-------------

The module has no menu or modifiable settings. Configuration is done on a field by field
basis on the Manage display page by selecting the *Link Title* formatter for any link field.

